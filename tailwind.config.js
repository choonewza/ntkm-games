/** @type {import('tailwindcss').Config} */
module.exports = {
  important: true,
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/app/**/*.{js,ts,jsx,tsx}",
    "node_modules/daisyui/dist/**/*.js",
    "node_modules/react-daisyui/dist/**/*.js",
  ],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        nt: "#FFC525",
        "nt-content": "#555B60",
        "dark-main": "#18191A",
        "dark-second": "#242526",
        "dark-third": "#3A3B3C",
        "dark-txt": "#B8BBBF",
      },
      translate: {},
    },
    minWidth: {
      "1/2": "50%",
    },
    maxHeight: {
      "9/10": "90%",
      "9.5/10": "95%",
      "10/10": "100%",
    },
  },
  plugins: [
    require("@tailwindcss/typography"),
    require("tailwind-scrollbar"),
    require("@tailwindcss/aspect-ratio"),
    require("daisyui"),
  ],
  variants: {
    scrollbar: ["rounded"],
  },

  daisyui: {
    styled: true,
    themes: [
      {
        mytheme: {
          primary: "#560DF8",
          secondary: "#3354f7",
          accent: "#46fcb0",
          neutral: "#24273D",
          "base-100": "#FFFFFF",
          "base-200": "#F3F4F6",
          info: "#9ACFE4",
          success: "#1B6A5D",
          warning: "#F1B965",
          error: "#EF5252",

          "--rounded-box": "1rem", // border radius rounded-box utility class, used in card and other large boxes
          "--rounded-btn": "0.5rem", // border radius rounded-btn utility class, used in buttons and similar element
          "--rounded-badge": "1.9rem", // border radius rounded-badge utility class, used in badges and similar
          "--animation-btn": "0.25s", // duration of animation when you click on button
          "--animation-input": "0.2s", // duration of animation for inputs like checkbox, toggle, radio, etc
          "--btn-text-case": "uppercase", // set default text transform for buttons
          "--btn-focus-scale": "0.95", // scale transform of button when you focus on it
          "--border-btn": "1px", // border width of buttons
          "--tab-border": "1px", // border width of tabs
          "--tab-radius": "0.5rem", // border radius of tabs
        },
      },
    ],
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    prefix: "",
    // darkTheme: "dark",
  },

  // corePlugins: {
  //   preflight: false,
  // }
};
