/** @type {import('next').NextConfig} */
const nextConfig = {
  basePath: '/ntgame',
  swcMinify :false,
  experimental: {
    appDir: true,
  },

  reactStrictMode: false,


  images: {
    // disableStaticImages: false,
    domains: [
      "localhost",
      "eportfolio.ntplc.co.th",
      "links.papareact.com",
      "platform-lookaside.fbsbx.com",
      "firebasestorage.googleapis.com",
      "placeimg.com",
      "192.168.0.21",
      "192.168.1.117",
      "flowbite.com"
    ],
  },
}

module.exports = nextConfig
