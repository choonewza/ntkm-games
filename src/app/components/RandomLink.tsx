"use client";

import { linkUrl } from "@/utils/link";
import { useRouter } from "next/navigation";
import type { ReactElement } from "react";

interface Props {className?:any,name?:string}
function RandomLink({className,name="Random"}: Props): ReactElement {
  const router = useRouter();
  
  const handleRandom = () => {
    const url = randomLink();
    console.log("Random...",url)
    return router.push(linkUrl(randomLink()))
  }
  return (
    <button onClick={handleRandom}className={className}>{name}</button>
  );
}

export default RandomLink;

const randomLink = () => {
  const links = [
    "/puzzle/puzzle-1",
    "/puzzle/puzzle-2",
    "/puzzle/puzzle-3",
    "/quiz-1",
    "/quiz-2",
    "/quiz-3",
  ];
  const index = Math.floor(Math.random() * 6);
  return links[index];
};
