

export default function Home() {
  return (
    <main className="flex items-center justify-center">
      <h1 className="text-7xl font-bold">NT GAME</h1>
    </main>
  )
}
