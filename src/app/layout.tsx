import "./globals.css";
import { HTML5Backend } from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";
import { Navbar, Menu, Button } from "react-daisyui";
import Link from "next/link";
import { linkUrl } from "@/utils/link";
import RandomLink from "./components/RandomLink";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head />
      <body>
        {/* <DndProvider backend={HTML5Backend}>{children}</DndProvider> */}
        <div className="navbar bg-neutral text-neutral-content">
          <div className="flex-1">
            <a className="text-xl normal-case btn btn-ghost">NT KM</a>
          </div>
          <div className="flex-none">
            <ul className="px-1 menu menu-horizontal">
              <li>
                <RandomLink/>
              </li>
              <li>
                <Link href={linkUrl("/puzzle/puzzle-1")}>Puzzle 1</Link>
              </li>
              <li>
                <Link href={linkUrl("/puzzle/puzzle-2")}>Puzzle 2</Link>
              </li>
              <li>
                <Link href={linkUrl("/puzzle/puzzle-3")}>Puzzle 3</Link>
              </li>
              <li>
                <Link href={linkUrl("/quiz-1")}>Quiz 1</Link>
              </li>
              <li>
                <Link href={linkUrl("/quiz-2")}>Quiz 2</Link>
              </li>
              <li>
                <Link href={linkUrl("/quiz-3")}>Quiz 3</Link>
              </li>
            </ul>
          </div>
        </div>
        {children}
      </body>
    </html>
  );
}

const randomLink = () => {
  const links = [
    "/puzzle/puzzle-1",
    "/puzzle/puzzle-2",
    "/puzzle/puzzle-3",
    "/quiz-1",
    "/quiz-2",
    "/quiz-3",
  ];
  const index = Math.floor(Math.random() * 6);
  return links[index];
};
