import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from './page.module.css'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import Quiz from './Quiz'


export default function UrlNtkmQuizPage() {
  return (
    <main className="h-screen">
      <Quiz/>
    </main>
  )
}
