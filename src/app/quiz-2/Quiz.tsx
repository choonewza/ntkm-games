"use client";

import { linkUrl } from "@/utils/link";
import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { Button, Modal } from "react-daisyui";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import Swal from "sweetalert2";
import RandomLink from "../components/RandomLink";
import Spinner from "../components/Spinner";

interface AnswerItemProps {
  id: string;
  name: string;
  result: boolean;
}

const defaultAnswers: AnswerItemProps[] = [
  {
    id: "1",
    name: "https://eport.ntplc.co.th",
    result: false,
  },
  {
    id: "2",
    name: "https://eportfolio.ntplc.com",
    result: false,
  },
  {
    id: "3",
    name: "https://eportfolio.ntplc.co.th",
    result: true,
  },
  {
    id: "4",
    name: "https://eport.ntplc.com",
    result: false,
  },
];

const getAnswers = () => {
  const shuffledArray = defaultAnswers.sort((a, b) => 0.5 - Math.random());
  return shuffledArray;
};
export default function Quiz() {
  const [result, setResult] = useState<"pass" | "fail" | null>(null);
  const [answers, setAnswers] = useState<AnswerItemProps[]>([]);
  useEffect(() => {
    setAnswers(getAnswers());
  }, []);

  const reset = () => {
    // window.location.reload();
    setResult(null);
    setAnswers(getAnswers());
  };

  const checkAnswer = (row: AnswerItemProps) => {
    if (row.result) {
      setResult("pass");
    } else {
      setResult("fail");
    }
  };

  if (answers.length === 0) {
    return (
      <div className="flex items-center justify-center h-screen text-gray-900 bg-yellow-500">
        <div className="w-[1000px] max-w-full pb-16 flex justify-center items-center gap-5">
          <Spinner size={16} />
          <div className="text-2xl font-bold">กำลังโหลดเกม...</div>
        </div>
      </div>
    );
  }

  return (
    <div>
      <div className="flex items-start justify-center h-screen pt-16 text-gray-900 bg-yellow-500">
        <div className="w-[1000px] max-w-full pb-16">
          <h1 className="mb-8 text-3xl font-bold text-center text-blue-900 md:mb-16 md:text-5xl lg:text-6xl">
            ชื่อเว็บไซต์ E-Portfolio คืออะไร?
          </h1>
          <div className="flex flex-col px-3 space-y-6">
            {answers.map((row) => (
              <Button
                key={row.id}
                onClick={() => checkAnswer(row)}
                color="accent"
                className="flex items-center justify-center w-full h-16 text-2xl font-bold normal-case bg-orange-100 border rounded-full md:text-5xl md:h-24"
              >
                {row.name}
              </Button>
            ))}
          </div>
        </div>
      </div>

      {result === "pass" && (
        <Modal
          open={true}
          className="px-6 py-8 lg:py-16 max-w-7xl self-center max-h-10/10 md:max-h-9.5/10 overflow-x-hidden overflow-y-auto bg-green-200"
        >
          <Modal.Body className="text-gray-900 ">
            <h1 className="mb-6 text-3xl font-bold text-center md:text-5xl lg:text-6xl md:mb-10 text-primary">
              <span className="text-green-700 underline animate-pulse">
                คุณตอบถูก
              </span>{" "}
              ชื่อเว็บไซต์ E-Portfolio คือ
            </h1>
            <div className="gap-5 md:flex md:justify-center">
              <h1 className="flex items-center justify-center w-full p-6 text-xl font-bold text-center bg-purple-200 shadow-xl md:w-2/3 md:text-3xl lg:text-5xl text-primary rounded-box">
                &quot;https://eportfolio.ntplc.co.th&quot;
              </h1>
              <div className="flex justify-center w-full mt-0 md:w-1/3 md:mt-10 ">
                <picture>
                  <img
                    className=" w-60 md:w-60 lg:w-80"
                    src={linkUrl("/images/nt_mascot/ผ่าน.png")}
                    alt="img"
                  />
                </picture>
              </div>
            </div>
          </Modal.Body>

          <Modal.Actions className="flex items-center justify-center mt-4 md:mt-10">
            <RandomLink
              name="เริ่มเกมใหม่"
              className="py-4 border rounded-full border-gray-600 shadow font-bold px-10 text-3xl text-gray-900 bg-yellow-500 hover:bg-gray-900 hover:text-yellow-500"
            />
          </Modal.Actions>
        </Modal>
      )}

      {result === "fail" && (
        <Modal
          open={true}
          className="px-6 py-8 lg:py-16 max-w-7xl self-center max-h-10/10 md:max-h-9.5/10 overflow-x-hidden overflow-y-auto bg-red-200"
        >
          <Modal.Body className="text-gray-900 ">
            <h1 className="mb-6 text-3xl font-bold text-center md:text-5xl lg:text-6xl md:mb-10 text-primary">
              <span className="text-red-700 underline animate-pulse">
                คุณตอบผิด
              </span>{" "}
              ชื่อเว็บไซต์ E-Portfolio คือ
            </h1>
            <div className="gap-5 md:flex md:justify-center">
              <h1 className="flex items-center justify-center w-full p-6 text-xl font-bold text-center bg-purple-200 shadow-xl md:w-2/3 md:text-3xl lg:text-5xl text-primary rounded-box">
                &quot;https://eportfolio.ntplc.co.th&quot;
              </h1>
              <div className="flex justify-center w-full mt-4 md:w-1/3 md:mt-8 ">
                <picture>
                  <img
                    className=" w-60 md:w-60 lg:w-80"
                    src={linkUrl(
                      "/images/nt_mascot/robot_nt_อีกนิดเดียวเอง.png"
                    )}
                    alt="img"
                  />
                </picture>
              </div>
            </div>
          </Modal.Body>

          <Modal.Actions className="flex items-center justify-center mt-4 md:mt-10">
            <RandomLink
              name="เริ่มเกมใหม่"
              className="py-4 border rounded-full border-gray-600 shadow font-bold px-10 text-3xl text-gray-900 bg-yellow-500 hover:bg-gray-900 hover:text-yellow-500"
            />
          </Modal.Actions>
        </Modal>
      )}
    </div>
  );
}
