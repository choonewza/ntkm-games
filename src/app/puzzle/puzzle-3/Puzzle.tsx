"use client";

import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { Button, Modal } from "react-daisyui";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import Swal from "sweetalert2";
import Bin from "../Bin";
import Box from "../Box";
import Spinner from "../../components/Spinner";
import Timer from "../Timer";
import { linkUrl } from "@/utils/link";
import RandomLink from "@/app/components/RandomLink";

interface WordItemProps {
  id: string;
  name: string;
}

const defaultWords: WordItemProps[] = [
  {
    id: "1",
    name: "คณะกรรมการด้านการจัดการความรู้และนวัตกรรม ระดับองค์กร",
  },
  {
    id: "2",
    name: "คณะทำงานด้านการจัดการความรู้และนวัตกรรม ประจำสายงาน",
  },
  {
    id: "3",
    name: "คณะทำงานย่อย หรือ KM & IM Micro Team",
  },
];

const getWords = () => {
  const shuffledArray = defaultWords.sort((a, b) => 0.5 - Math.random());
  return shuffledArray;
};
export default function Puzzle() {
  const timerRef = useRef<any>();

  const [isSuccess, setIsSuccess] = useState(false);
  const [time, setTime] = useState(0);
  const [countMatch, setCountMatch] = useState(0);
  const [words, setWords] = useState<WordItemProps[]>([]);
  useEffect(() => {
    setWords(getWords());
  }, []);

  const reset = () => {
    window.location.reload();
  };

  if (words.length === 0) {
    return (
      <div className="flex items-center justify-center h-screen text-gray-900 bg-yellow-500">
        <div className="w-[1000px] max-w-full pb-16 flex justify-center items-center gap-5">
          <Spinner size={16} />{" "}
          <div className="text-5xl font-bold">กำลังโหลดเกม...</div>
        </div>
      </div>
    );
  }

  if (isSuccess === false && countMatch >= words.length) {
    setIsSuccess(true);
    setTime(timerRef.current.getTime());
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <div className="flex items-start justify-center h-screen pt-16 text-gray-900 bg-yellow-500">
        <div className="w-[1000px] max-w-full pb-16">
          <h1 className="mb-20 text-6xl font-bold text-center text-black">
            เรียงลำดับโครงสร้างการจัดการความรู้และนวัตกรรมของ NT
          </h1>
          <div className="flex justify-between">
            <div className="flex flex-col items-start justify-start w-full px-6 space-y-4">
              {words.map((row: WordItemProps, index: number) => (
                <Box
                  key={row.id}
                  matching={row.id}
                  name={row.name}
                  setCountMatch={setCountMatch}
                  className="text-2xl"
                />
              ))}
            </div>
            <div className="flex flex-col items-center justify-center space-y-6 w-80">
              <picture>
                <img
                  src={linkUrl("/images/arrow-png-hd.png")}
                  alt="arrow"
                  className="w-28"
                />
              </picture>
              <picture>
                <img
                  src={linkUrl("/images/nt_mascot/robot_nt.ขอด่วนเลยตรับ.jpg")}
                  alt="arrow"
                  className="rounded-full"
                />
              </picture>
            </div>
            <div className="flex flex-col items-start justify-start w-full px-6 space-y-4">
              {Array.from(Array(words.length).keys()).map((row, index) => (
                <Bin
                  key={row}
                  matching={String(index + 1)}
                  className="text-2xl"
                />
              ))}
            </div>
          </div>
          <div className="flex items-center justify-center gap-5 mt-8">
            {isSuccess === false && <Timer ref={timerRef} />}
            <Button
              size="lg"
              onClick={reset}
              className="text-3xl text-white shadow-lg w-80"
            >
              เริ่มเกมใหม่
            </Button>
          </div>
        </div>
      </div>

      {isSuccess && (
        <Modal
          open={true}
          className="px-6 p-10 max-w-7xl self-start max-h-10/10 md:self-center md:max-h-9.5/10 overflow-x-hidden overflow-y-auto"
        >
          <Modal.Header className="text-4xl font-bold text-center text-primary">
            ลำดับโครงสร้างการจัดการความรู้และนวัตกรรมของ NT
          </Modal.Header>

          <Modal.Body className="pt-6 text-gray-900">
            <div className="flex items-center justify-center gap-5 p-6 font-bold text-center bg-purple-200 text-primary rounded-box">
              <div className="p-3 text-2xl bg-white shadow rounded-box w-60">
                คณะกรรมการด้านการจัดการความรู้และนวัตกรรม ระดับองค์กร
              </div>
              <picture>
                <img
                  src={linkUrl("/images/arrow-png-hd.png")}
                  alt="arrow"
                  className="w-20"
                />
              </picture>
              <div className="p-3 text-2xl bg-white shadow rounded-box w-60">
                คณะทำงานด้านการจัดการความรู้และนวัตกรรม ประจำสายงาน
              </div>
              <picture>
                <img
                  src={linkUrl("/images/arrow-png-hd.png")}
                  alt="arrow"
                  className="w-28"
                />
              </picture>
              <div className="p-3 text-2xl bg-white shadow rounded-box w-60">
                คณะทำงานย่อย หรือ KM & IM Micro Team
              </div>
            </div>
            <div className="flex items-center justify-center mt-6">
              <picture>
                <img
                  src={linkUrl("/images/nt_mascot/robot_nt_goodjob.png")}
                  alt="arrow"
                  className="w-80"
                />
              </picture>
              <div className="mt-10 font-bold text-center text-red-500 text-7xl ">
                ใช้เวลา <span className="animate-pulse">{time}</span> วินาที
              </div>
            </div>
          </Modal.Body>

          <Modal.Actions className="flex items-center justify-center mt-10">
            <RandomLink
              name="เริ่มเกมใหม่"
              className="py-4 border rounded-full border-gray-600 shadow font-bold px-10 text-3xl text-gray-900 bg-yellow-500 hover:bg-gray-900 hover:text-yellow-500"
            />
          </Modal.Actions>
        </Modal>
      )}
    </DndProvider>
  );
}
