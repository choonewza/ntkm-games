"use client";

import { useDrag } from "react-dnd";
import type { DragSourceMonitor } from "react-dnd";
import { useState } from "react";
import { ItemTypes } from "./ItemTypes";
import classNames from "classnames";
// import ringer from "./audio1.mp3";

interface DropResult {
  allowedDropEffect: string;
  dropEffect: string;
  name: string;
  matching: string;
}

interface Props {
  name: string;
  matching: string;
  setCountMatch: any
  className?: any;
}

export default function Box({ name, matching,setCountMatch,className }: Props) {
  const [label, setLabel] = useState(name);
  const [isTrue, setIsTrue] = useState(false);

  // const audio = new Audio(ringer);
  // audio.loop = true;

  const [{ opacity }, drag] = useDrag(() => {
    return {
      type: ItemTypes.BOX,
      item: { name, matching },
      end(item: any, monitor) {
        const dropResult: any = monitor.getDropResult() as DropResult;
        if (dropResult !== null && dropResult.isMatching) {
          setIsTrue(true);
          setCountMatch((prev:number)=>prev+1);
        }
        // if (item && dropResult) {
        //     let alertMessage = ''
        //     const isDropAllowed = dropResult.isMatching;

        //     if (isDropAllowed) {
        //         const isCopyAction = dropResult.dropEffect === 'copy'
        //         const actionName = isCopyAction ? 'copied' : 'moved'
        //         alertMessage = `You ${actionName} ${item.name} into ${dropResult.name}!`
        //     } else {
        //         alertMessage = `You cannot ${dropResult.dropEffect} an item into the ${dropResult.name}`
        //     }
        //     // alert(alertMessage)
        // }
      },
      collect: (monitor: DragSourceMonitor) => ({
        opacity: monitor.isDragging() ? 0.4 : 1,
      }),
    };
  }, [name]);

  return (
    <div
      ref={drag}
      className={classNames("flex items-center w-full h-20 p-3 text-4xl bg-blue-100 border border-dashed cursor-pointer rounded-box text-primary",className)}
      style={{ opacity: isTrue ? 0.4 : opacity }}
    >
      <div className="w-full text-center">{label}</div>
    </div>
  );
}
