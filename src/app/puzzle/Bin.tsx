"use client";
import classNames from "classnames";
import { useState } from "react";
import { useDrop } from "react-dnd";
import { ItemTypes } from "./ItemTypes";

interface Props {
  matching: string;
  className?: any;
}

export default function Bin({ matching, className }: Props) {
  const [label, setLabel] = useState("");
  const [isTrue, setIsTrue] = useState(false);

  const [{ canDrop, isOver }, drop] = useDrop(
    () => ({
      accept: ItemTypes.BOX,
      drop: (_item: any, monitor) => {
        if (_item.matching === matching) {
          setLabel(_item.name);
          setIsTrue(true);
          return {
            isMatching: true,
          };
        } else {
          return {
            isMatching: false,
          };
        }
      },
      collect: (monitor: any) => ({
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop(),
      }),
    }),
    [matching]
  );

  const isActive = canDrop && isOver;
  const backgroundColor = selectBackgroundColor(isActive, canDrop);
  return (
    <div
      ref={drop}
      className={classNames(
        "border text-4xl border-dashed  w-full shadow-box p-3 rounded-box h-20 flex items-center  " +
          (isTrue
            ? " bg-green-200 text-primary shadow-lg "
            : " text-primary " + backgroundColor),
        className
      )}
    >
      <div className="w-full font-bold text-center">{label}</div>
    </div>
  );
}

function selectBackgroundColor(isActive: boolean, canDrop: boolean) {
  if (isActive) {
    return "bg-red-400";
  } else if (canDrop) {
    return "bg-green-200";
  } else {
    return "bg-white";
  }
}
