"use client";

import Image from "next/image";
import { useEffect, useRef, useState } from "react";
import { Button, Modal } from "react-daisyui";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import Swal from "sweetalert2";
import Bin from "../Bin";
import Box from "../Box";
import Spinner from "../../components/Spinner";
import Timer from "../Timer";
import { linkUrl } from "@/utils/link";
import RandomLink from "@/app/components/RandomLink";

interface WordItemProps {
  id: string;
  name: string;
}

const defaultWords: WordItemProps[] = [
  {
    id: "1",
    name: "การบ่งชี้ความรู้",
  },
  {
    id: "2",
    name: "การสร้างและแสวงหาความรู้",
  },
  {
    id: "3",
    name: "การจัดความรู้ให้เป็นระบบ",
  },
  {
    id: "4",
    name: "การประมวลและกลั่นกรองความรู้",
  },
  {
    id: "5",
    name: "การเข้าถึงความรู้",
  },
  {
    id: "6",
    name: "การแบ่งปันแลกเปลี่ยนความรู้",
  },
  {
    id: "7",
    name: "การเรียนรู้",
  },
];

const getWords = () => {
  const shuffledArray = defaultWords.sort((a, b) => 0.5 - Math.random());
  return shuffledArray;
};
export default function Puzzle() {
  const timerRef = useRef<any>();

  const [isSuccess, setIsSuccess] = useState(false);
  const [time, setTime] = useState(0);
  const [countMatch, setCountMatch] = useState(0);
  const [words, setWords] = useState<WordItemProps[]>([]);

  useEffect(() => {
    setWords(getWords());
  }, []);

  const reset = () => {
    window.location.reload();
  };

  if (words.length === 0) {
    return (
      <div className="flex items-center justify-center h-screen text-gray-900 bg-yellow-500">
        <div className="w-[1000px] max-w-full pb-16 flex justify-center items-center gap-5">
          <Spinner size={16} />{" "}
          <div className="text-5xl font-bold">กำลังโหลดเกม...</div>
        </div>
      </div>
    );
  }

  if (isSuccess === false && countMatch >= words.length) {
    setIsSuccess(true);
    setTime(timerRef.current.getTime());
  }

  return (
    <DndProvider backend={HTML5Backend}>
      <div className="flex items-start justify-center h-screen pt-10 text-gray-900 bg-yellow-500">
        <div className="w-[1000px] max-w-full pb-16">
          <div className="mb-10 text-3xl font-bold text-left text-black">
            <div className="mb-5 text-center underline">
              กระบวนการจัดการความรู้ (KM Process)
            </div>
            <div>
              เป็นกระบวนการที่จะช่วยให้เกิดพัฒนาการของความรู้
              หรือการจัดการความรู้ที่จะเกิดขึ้นภายในองค์กร มีทั้งหมด 7 ขั้นตอน
              จงเรียงลำดับงาน
            </div>
          </div>
          <div className="flex justify-between">
            <div className="flex flex-col items-start justify-start w-full px-6 space-y-4">
              {words.map((row: WordItemProps, index: number) => (
                <Box
                  key={row.id}
                  matching={row.id}
                  name={row.name}
                  setCountMatch={setCountMatch}
                  className="text-2xl h-14"
                />
              ))}
            </div>
            <div className="flex flex-col items-center justify-center space-y-6 w-80">
              <picture>
                <img
                  src={linkUrl("/images/arrow-png-hd.png")}
                  alt="arrow"
                  className="w-28"
                />
              </picture>
              <picture>
                <img
                  src={linkUrl("/images/nt_mascot/robot_nt.ขอด่วนเลยตรับ.jpg")}
                  alt="arrow"
                  className="rounded-full"
                />
              </picture>
            </div>
            <div className="flex flex-col items-start justify-start w-full px-6 space-y-4">
              {Array.from(Array(words.length).keys()).map((row, index) => (
                <Bin
                  key={row}
                  matching={String(index + 1)}
                  className="text-2xl h-14"
                />
              ))}
            </div>
          </div>

          <div className="flex items-center justify-center gap-5 mt-8">
            {isSuccess === false && <Timer ref={timerRef} />}
            <Button
              size="lg"
              onClick={reset}
              className="text-3xl text-white shadow-lg w-80"
            >
              เริ่มเกมใหม่
            </Button>
          </div>
        </div>
      </div>

      {isSuccess && (
        <Modal
          open={true}
          className="px-6 p-8 max-w-7xl self-start max-h-10/10 md:self-center md:max-h-9.5/10 overflow-x-hidden overflow-y-auto"
        >
          <Modal.Header className="text-2xl font-bold text-center text-primary">
            <div className="mb-5 text-3xl underline">
              กระบวนการจัดการความรู้ (Knowledge Process)
            </div>
            <div>
              เป็นกระบวนการที่จะช่วยให้เกิดพัฒนาการของความรู้
              หรือการจัดการความรู้ที่จะเกิดขึ้นภายในองค์กร มีทั้งหมด 7 ขั้นตอน
              จงเรียงลำดับงาน
            </div>
          </Modal.Header>

          <Modal.Body className="pt-0 text-gray-900">
            <div className="p-6 text-xl font-bold text-left bg-purple-200 text-primary rounded-box">
              <ul className="pl-10 list-decimal">
                <li>การบ่งชี้ความรู้ (Knowledge Identification)</li>
                <li>การสร้างและแสวงหาความรู้ (Knowledge Creation and Acquisition)</li>
                <li>การจัดความรู้ให้เป็นระบบ (Knowledge Organization)</li>
                <li>การประมวลและกลั่นกรองความรู้ (Knowledge Codification and Refinement)</li>
                <li>การเข้าถึงความรู้ (Knowledge Access)</li>
                <li>การแบ่งปันแลกเปลี่ยนความรู้ (Knowledge Sharing)</li>
                <li>การเรียนรู้ (Learning)</li>
              </ul>
            </div>
            <div className="flex items-center justify-center mt-6">
              <picture>
                <img
                  src={linkUrl("/images/nt_mascot/robot_nt_goodjob.png")}
                  alt="arrow"
                  className=" w-52"
                />
              </picture>
              <div className="mt-10 text-4xl font-bold text-center text-red-500 ">
                ใช้เวลา <span className="animate-pulse">{time}</span> วินาที
              </div>
            </div>
          </Modal.Body>

          <Modal.Actions className="flex items-center justify-center mt-4">
            <RandomLink
              name="เริ่มเกมใหม่"
              className="py-4 border rounded-full border-gray-600 shadow font-bold px-10 text-3xl text-gray-900 bg-yellow-500 hover:bg-gray-900 hover:text-yellow-500"
            />
          </Modal.Actions>
        </Modal>
      )}
    </DndProvider>
  );
}
