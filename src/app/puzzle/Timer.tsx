"use client";

import { useDrag } from "react-dnd";
import type { DragSourceMonitor } from "react-dnd";
import { forwardRef, useEffect, useImperativeHandle, useState } from "react";
import { ItemTypes } from "./ItemTypes";
// import ringer from "./audio1.mp3";
interface Props {}

const Timer = forwardRef(({}: Props, ref) => {
  const [timeLeft, setTimeLeft] = useState(0);

  // useEffect(() => {
  //   // exit early when we reach 0
  //   if (!timeLeft) return;

  //   // save intervalId to clear the interval when the
  //   // component re-renders
  //   const intervalId = setInterval(() => {
  //     setTimeLeft(timeLeft - 1);
  //   }, 1000);

  //   // clear interval on re-render to avoid memory leaks
  //   return () => clearInterval(intervalId);
  //   // add timeLeft as a dependency to re-rerun the effect
  //   // when we update it
  // }, [timeLeft]);

  useImperativeHandle(ref, () => ({
    getTime() {
      return timeLeft;
    },
  }));

  useEffect(() => {
    // exit early when we reach 0
    // if (!timeLeft) return;

    // save intervalId to clear the interval when the
    // component re-renders
    const intervalId = setInterval(() => {
      setTimeLeft((prev) => prev + 1);
    }, 1000);

    // clear interval on re-render to avoid memory leaks
    return () => clearInterval(intervalId);
    // add timeLeft as a dependency to re-rerun the effect
    // when we update it
  }, [timeLeft]);
  return (
    <div className="flex items-center justify-center ">
      <div className="p-3 text-3xl font-bold text-center text-red-700 bg-white shadow-lg rounded-xl">
        {timeLeft} วินาที
      </div>
    </div>
  );
});

Timer.displayName = "Editor";

export default Timer;
