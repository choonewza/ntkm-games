import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from './page.module.css'
import Puzzle from './Puzzle'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'


export default function Home() {
  return (
    <main className="h-screen">
      <Puzzle />
    </main>
  )
}
